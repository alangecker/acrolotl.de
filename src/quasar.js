import Vue from 'vue'

import './styles/quasar.styl'
import 'quasar/dist/quasar.ie.polyfills'
import lang from 'quasar/lang/de.js'
import '@quasar/extras/material-icons/material-icons.css'
import {
  Quasar, 
  QBtn,
  QIcon,
  QList,
  QItem,
  QItemSection,
  QItemLabel,
  QInput,
  QSelect,
  QCheckbox,
  QRadio,
  QToggle,
  QTooltip,
  QSpinner,
  QBanner,
  Notify
} from 'quasar'

Vue.use(Quasar, {
  config: {},
  components: {
    QBtn,
    QIcon,
    QList,
    QItem,
    QItemSection,
    QItemLabel,
    QInput,
    QSelect,
    QCheckbox,
    QRadio,
    QToggle,
    QTooltip,
    QSpinner,
    QBanner
  },
  directives: {
  },
  plugins: {
    Notify
  },
  lang: lang
 })