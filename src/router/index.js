import Vue from 'vue'
import VueRouter from 'vue-router'
import Infos from '../components/Infos.vue'
import Registration from '../components/Registration.vue'
import Imprint from '../components/Imprint.vue'
import Confirm from '../components/Confirm.vue'


Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Infos
  },
  {
    path: '/anmeldung',
    name: 'registration',
    component: Registration
  },
  {
    path: '/impressum',
    name: 'imprint',
    component: Imprint
  },
  {
    path: '/confirm/:key',
    name: 'confirm',
    component: Confirm
  },
  // {
  //   path: '/about',
  //   name: 'about',
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  // }
]

const router = new VueRouter({
  routes
})

export default router
